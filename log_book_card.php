<div class="card" style="width: 18rem;">
    <div class="card-body">
        <h5 class="card-title"> WEEK <php echo $week ;?>
        </h5>
        <h6 class="card-subtitle mb-2 text-muted">
            <php echo $day?>
        </h6>
        <p class="card-text">
            <php echo $workdone?>
        </p>
        <a href="#" class="card-link">Approve</a>
        <a href="#" class="card-link">Add Comment</a>
    </div>
</div>