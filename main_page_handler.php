<?php
error_reporting(~E_NOTICE); // avoid notice
use App\Database;

use function App\get_db;

$main_page = true;

require("required/Database.php");
require("required/utils.php");
$conn =  new Database();
$session = Session::getInstance();
if (!$session->user) {
	redirect("login.php");
}

if (isset($_POST['grade_log'])) {
	$data = escape_post_data();
	var_dump($data);
	$matric = $data['matric'];
	$week = $data['week'];
	$grade = ["grade" => $data['grade']];
	get_db()->update("reports", $grade, "week='$week' and matric ='$matric'");
	$sql = "SELECT * FROM reports where matric='$matric' and week='$week'";

	$get_logs = get_db()->select($sql);
	if ($get_logs) {
		$logbooks = format_logs($get_logs);
		$grade = false;
		// var_dump($logbooks);

	}
}

$user = $session->user;
if (is_a_student()) {
	$get_supervisor = $conn->select("SELECT * FROM supervisors WHERE username= '" . $user['supervisor_id'] . "'");
	$get_Img = $conn->select("SELECT matric, userPic FROM tbl_users WHERE matric= '" . $user['matric'] . "' order by userID desc limit 1");


	if ($get_Img) {
		$userpic = $get_Img[0];
		if (file_exists("user_images/" . $userpic['userPic'])) {
			$profile_image = "user_images/" . $userpic['userPic'];
			// echo $profile_image;
		} else {
			$profile_image = null;
		}
	}
	if ($get_supervisor) {
		$user_supervisor = $get_supervisor[0];
	}
}



function format_logs($get_logs)
{
	return array_map(function ($logs) {

		$data = [
			"week" => $logs['week'],

			"Monday" => [$logs['mondata'], $logs["mondate"]],
			"Tuesday" => [$logs['tuesdata'], $logs['tuesdate']],
			"Wednesday" => [$logs['wednesdata'], $logs['wednesdate']],
			"Thursday" => [$logs['thursdata'], $logs['thursdate']],
			"Friday" => [$logs["fridata"], $logs['fridate']],
			"Saturday" => [$logs['saturdata'], $logs['saturdate']]
		];
		if (is_supervisor()) {
			$data['grade'] = $logs['grade'];
		}
		return $data;
	}, $get_logs);
}

$page = $_SERVER['REQUEST_URI'];

function is_view_logbook($page)
{
	return str_contains($page, "mylogbooks") || str_contains($page, "viewlogbook.php");
}

function is_fill_logbook($page)
{
	return str_contains($page, "/logbook.php");
}


if (is_view_logbook($page)) {
	if (isset($_POST['view-logs'])) {
		$week = $_POST['week'];
		$matric = $user['matric'] ?? $_POST['matric'];
		Session::getInstance()->session_matric = $matric;

		if (strtolower($week) == 'all') {
			$sql = "SELECT * FROM reports where matric='$matric' order by week";
		} else {
			$sql = "SELECT * FROM reports where matric='$matric' and week='$week'";
		}
		$get_logs = get_db()->select($sql);
		if ($get_logs) {
			$logbooks = format_logs($get_logs);
		}
	}
}

if (request_is("POST") && is_fill_logbook($page)) {
	// echo $page;

	$data = escape_post_data();
	$week = $data['week'];
	$day = $data['day'];
	$matric = Session::getInstance()->user['matric'];
	$daydata = explode("day", strtolower($day))[0] . "data";
	$daydate =  explode("day", strtolower($day))[0] . "date";
	$data_to_save = [
		"matric" => $matric,
		"week" => $week,
		strtolower($day) => $day,
		$daydata => $data['workdone'],
		$daydate => $data['date']

	];
	$db = get_db();
	$has_filled_for_the_week = $db->select("select * from reports where matric = '$matric' and week = '$week'");
	if ($has_filled_for_the_week) {
		$saved = $db->update("reports", $data_to_save, "matric = '$matric' and week = '$week'");
	} else {
		$saved = $db->insert("reports", $data_to_save);
	}
	if ($saved) {
		$successMSG = "save succesfully";
	} else {
		$errMSG = "error while saving your data try again latter";
	}
}


function get_logs($week, $matric)
{
	return get_db()->select(
		"select * from reports where week=:week and matric=:matric",
		["week" => $week, "matric" => $matric]
	);
}
if (request_is("GET") && is_view_logbook($page) && isset($_GET['action'])) {
	// if(!is_a_student() || !is_supervisor()){
	// 	redirect("login.php?type=st");
	// }

	$action = $_GET['action'];

	if ($action == "grade") {
		if (!is_supervisor()) {
			redirect("login.php?type=st");
		} else {
			$grade = true;
			// $matric = '';
			$week = $_GET['week'];
			$matric = Session::getInstance()->session_matric;
		}
	} else {
		$week = $_GET['week'];
		$day = strtolower($_GET['day']);
		$data_to_edit = explode("day", $day)[0] . "data";
		$date = explode("day", $day)[0] . "date";
		$matric = Session::getInstance()->user['matric'];
		$actions = [


			"edit" => function () use ($week, $day, $data_to_edit, $date, $matric) {

				if (count($logbook = get_logs($week, $matric)) > 0) {
					$result = [
						"day" => $day,
						"data_to_edit" => $data_to_edit,
						"workdone" => $logbook[0][$data_to_edit],
						"date_name" => $date,
						"date" => $logbook[0][$date],
						"matric" => $matric,
						"week" => $week
					];
					return $result;
				}
			},
			"delete" => function () use ($week, $day, $data_to_edit, $date, $matric) {
				if (count($logbook = get_logs($week, $matric)) > 0) {
					$logbook = $logbook[0];
					$logbook[$date] = null;
					$logbook[$day] = null;
					$logbook[$data_to_edit] = null;
					$deleted = get_db()->update("reports", $logbook, "matric='$matric' and week='$week'");
					return $deleted != false;
				}
			}
		];

		$data =  $actions[$action]();

		if (is_array($data)) {
			$data_to_edit = $data;
		} elseif (is_bool($data)) {
			redirect("mylogbooks.php?message=deleted sucessfully");
		}
	}
}



if (isset($_POST['btnsave'])) {
	$matric = $user['matric'];


	$imgFile = $_FILES['user_image']['name'];
	$tmp_dir = $_FILES['user_image']['tmp_name'];
	$imgSize = $_FILES['user_image']['size'];
	$error = $_FILES['user_image']['error'];
	if ($error || empty($tmp_dir) || $tmp_dir == '' | empty($imgFile)) {
		$errMSG = "Seems the file you selected does not exists. Please Select Image File.";
		redirect("mypage.php?error=$errMSG");
	}

	$upload_dir = "user_images/"; // upload directory
	// var_dump($_FILES['user_image']);
	$successMSG = $tmp_dir;

	$imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); // get image extension

	// valid image extensions
	$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions

	// rename uploading image
	$userpic = rand(1000, 1000000) . "." . $imgExt;
	if (!in_array($imgExt, $valid_extensions)) {
		$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		redirect("mypage.php?error=$errMSG");
	}
	if ($imgSize > 6000000) {
		$errMSG = "Sorry, your file is too large.";
		redirect("mypage.php?error=$errMSG");
	}

	$saved = move_uploaded_file($tmp_dir, $upload_dir . $userpic);
	if (!$saved) {
		$errMSG = "error image could not be saved....";
		redirect("mypage.php?error=$errMSG");
	}
	$saved_to_db = save_image($conn, $matric, $userpic);
	if (!$saved_to_db) {
		$errMSG = "error while uploading....";
		redirect("mypage.php?error=$errMSG");
	}

	$successMSG = "Passport Uploaded Successfully ...";
	header("refresh:5;mypage.php"); // redirects image view page after 5 seconds.

}


if (isset($_POST['edit']) && is_view_logbook($page)) {
	$data = escape_post_data();
	$data_to_update = [
		$data['data_to_edit'] => $data['workdone'],
		$data["date_name"] => $data['date']

	];
	$matric = $data['matric'];
	$week = $data['week'];
	$edited = get_db()->update("reports", $data_to_update, "week='$week' and matric='$matric'");
	if ($edited) {
		$get_logs = get_db()->select("select * from reports where week='$week' and matric ='$matric'");
		$logbooks = format_logs($get_logs);
	}
}

function is_search_job($page)
{
	return str_contains($page, "search_job");
}

if (request_is("GET") and is_search_job($page)) {
	$now = (new DateTime('now'))->format('Y-m-d H:i:s');
	$oneMonthAgo = (new \DateTime('1 month ago'))->format('Y-m-d H:i:s');
	$sql = "SELECT *  FROM `job_postings` WHERE `created_at` BETWEEN '$oneMonthAgo' AND '$now';";
	$jobs = get_db()->select($sql);
}
if (request_is("POST") and is_search_job($page)) {
}