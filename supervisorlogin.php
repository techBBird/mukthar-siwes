<?php

use App\Database;

require("required/Database.php");
require("required/utils.php");
$db = new Database();
$session = Session::getInstance();
$error =  [
    "invalid_username" => false,
    "empty_username" => false
];
?>

<?php

if (isset($_POST['username'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $error = [];
    // echo $username;

    if (!empty($username)) {

        $query = "SELECT * FROM supervisors WHERE username='$username' or email = '$username'";
        $get_supervisor = $db->select($query);
        if ($get_supervisor) {
            $supervisor = $get_supervisor[0];
            $password_hash = $supervisor['password'];
            $auth = password_verify($password, $password_hash);
            if ($auth) {
                $session->user = $supervisor;
                $session->isSupervisor = true;
                $session->isLogedIn = true;
                redirect("supervisor.php");
            } else {
                $error['invalid_username'] = true;
            }
        } else {
            $error["invalid_username"] =  true;
        }
    } else {
        $error['empty_username'] = true;
    }
}
?>

<?php require("topnav.php") ?>
<header id="head" class="secondary">
    <div class="container">
        <h1>LOG IN</h1>
        <p>Log In with your Staff ID Number!</p>
    </div>
</header>


<!-- container -->
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h3 class="section-title">login as supervisor</h3>

            <form class="form-light mt-20" action="supervisorlogin.php" method="post" role="form">
                <div class="form-group">
                    <?php if (isset($error['empty_username']) && $error['empty_username'] == true) : ?> <span
                        style='color:red;'><b>Please Enter Your Username or email!</b></span><?php endif ?>
                    <?php if (isset($error['invalid_username']) && $error['invalid_username'] == true) : ?> <span
                        style='color:red;'><b>Username or Password invalid</b></span><?php endif ?>
                    <label><img src="assets/images/staff.png" width="25px" height="25px"> Username</label>
                    <input name="username" id="id" type="text" class="form-control"
                        placeholder="Enter Your email or username to login">
                </div>
                <div class="form-group">
                    <label for="password"><img src="assets/images/staff.png" width="25px" height="25px">Password</label>
                    <input name="password" id="password" type="password" class="form-control"
                        placeholder="Enter Your Password">
                </div>

                <p><br /></p>
                <div class="text-center">
                    <button type="submit" class="btn btn-two">LOG IN</button>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>

    </div>
</div>
<!-- /container -->

<?php require("footer.php"); ?>