<!--
Author: WebThemez
Author URL: http://webthemez.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<?php $page_title = "ABOUT :: ONLINE SIWES LOG BOOK"?>
<?php include("required/utils.php"); ?>
<?php include("topnav.php")?>

<header id="head" class="secondary" name="head">
    <div class="container">
        <h1>About Us</h1>
    </div>
</header>


<!-- container -->
<section class="container">
    <div class="row">
        <!-- main content -->
        <section class="col-sm-8 maincontent">
            <h3>About Us</h3>
            <p>
            <ul>
                <ul>
                    <li>To evaluate all the processes involve during SIWES programme.</li>
                    <li>To design an Online interactive platform where school supervisor monitor the progress of the
                        students during their IT/SIWES training.</li>
                    <li>To design an Online interactive platform where students and supervisors communicate
                        regularly, this in turn enables the student to provide feedbacks regarding problems
                        encountered during his/her training program.</li>
                    <li>To evaluate the progress of the student during the IT/SIWES training</li>
                </ul>
                </p>
                <h3>Our Achievements</h3>
                <strong>2021</strong>
                <p>We delivered the best online </p>

        </section>
        <!-- /main -->

        <!-- Sidebar -->
        <aside class="col-sm-4 sidebar sidebar-right">

            <div class="panel">
                <h4>Latest News</h4>
                <ul class="list-unstyled list-spaces">
                    <li><a href="">Breaking News</a><br>
                        <span class="small text-muted">Student Current running project.</span>
                    </li>
                    <li><a href="">Resumption Date</a><br>
                        <span class="small text-muted">School resume by October</span>
                    </li>
                    <li><a href="">Department</a><br>
                        <span class="small text-muted">Computer</span>
                    </li>

                </ul>
            </div>

        </aside>
        <!-- /Sidebar -->

    </div>
</section>
<!-- /container -->



<footer id="footer">

    <div class="container">
        <div class="row">
            <div class="footerbottom">
                <div class="col-md-3 col-sm-6">
                    <div class="footerwidget">
                        <h4>
                            Project Objectives
                        </h4>
                        <div class="menu-course">
                            <ul class="menu">
                                <li>Speed</li>
                                <li>Neat Report</li>
                                <li>Less Paper Work</li>
                                <li>Any time Response</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footerwidget">
                        <h4>
                            Why do this?
                        </h4>
                        <div class="menu-course">
                            <ul class="menu">
                                <li>Make SIWES easy</li>
                                <li>No loss of data</li>
                                <li>No Log Book loss</li>
                                <li>Available always</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footerwidget">
                        <h4>
                            Browse through pages
                        </h4>
                        <div class="menu-course">
                            <ul class="menu">
                                <li><a href="index.php">
                                        Home
                                    </a>
                                </li>
                                <li> <a href="register.php">
                                        Register
                                    </a>
                                </li>
                                <li><a href="login.php">
                                        Log In
                                    </a>
                                </li>
                                <li>
                                    <a href="about.php">
                                        About
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footerwidget">
                        <h4>Contact</h4>
                        <p>Obafemi Awolowo University,Ile-Ife, Nigeria.</p>
                        <div class="contact-info">
                            <i class="fa fa-map-marker"></i> Ile-Ife Osun State<br>
                            <i class="fa fa-phone"></i>+234 (0) 8189065361 <br>
                            <i class="fa fa-envelope-o"></i> murtadhobalogun@student.oauife.edu.ng
                        </div>
                    </div><!-- end widget -->
                </div>
            </div>
        </div>
        <div class="social text-center">
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-dribbble"></i></a>
            <a href="#"><i class="fa fa-flickr"></i></a>
            <a href="#"><i class="fa fa-github"></i></a>
        </div>

        <div class="clear"></div>
        <!--CLEAR FLOATS-->
    </div>
    <div class="footer2">
        <div class="container">
            <div class="row">

                <div class="col-md-6 panel">
                    <div class="panel-body">
                        <p class="simplenav">
                            <a href="index.php">Home</a> |
                            <a href="register.php">Register</a> |
                            <a href="login.php">Log In</a> |
                            <a href="about.php">About</a>

                        </p>
                    </div>
                </div>

                <div class="col-md-6 panel">
                    <div class="panel-body">
                        <p class="text-right">
                            Copyright &copy; 2021. Designed by <b>Balogun Murtadho Abiodun - CSC/2016/123.</b>
                        </p>
                    </div>
                </div>

            </div>
            <!-- /row of panels -->
        </div>
    </div>


    <?php include("footer.php")?>