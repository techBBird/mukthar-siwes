<?php include("main_page_handler.php"); ?>
<?php
include("topnav.php");
$error = $_GET['error'] ?? null;

?>

<TABLE width="100%" cellpadding="0" cellspacing="0" border="0">
    <TR>
    <?php if (isset($_GET['message'])) : ?>
                            <p><span class="text-success"><b><?php echo $_GET['message'] ?></b></span></p><br />
                        <?php endif ?>

        <TD height="33" id="iPQLink" align="center">
            <div class="navi">
                <TABLE cellpadding="0" cellspacing="0" border="0" height="33">
                    <TR>

                        <TD width="114" align="center"><a href="mypage.php"
                                style="color:white; text-decoration:none">Dashboard</a></TD>
                        <TD width="10">|</TD>
                        <TD width="134" class="Hover" id="Link"><a href="logbook.php"
                                style="color:white; text-decoration:none">Fill Log Book</a></TD>
                        <TD width="10">|</TD>
                        <TD width="134" class="Hover" id="Link"><a href="mylogbooks.php"
                                style="color:white; text-decoration:none">Log Books</a></TD>
                        <TD width="10">|</TD>

                        <TD width="114" id="Link"><a href="search_job.php"
                                style="color:white; text-decoration:none">Search Job</a></TD>
                    </TR>
                </TABLE>
            </div>
        </TD>
    </TR>


    <TR>
        <TD height="364" align="center" style="padding-top:20pt">

            <TABLE cellspacing="0" cellpadding="0">
                <TR>
                    <TD width="550px" valign="top" id="FontPlain"
                        style="padding-right:10px; background-color:white; border:1px solid gray">
                        <p>&nbsp;</p>
                        <?php
                        echo "<span style='color:darkred; font-family:arial black; font-size:150%;'><b>&nbsp; Welcome, " . $user['surname'] . " " . $user['firstname'] . ".</b></span>"
                        ?>

                        <?php render_user_details($user, $user_supervisor ?? []) ?>
                        <p>&nbsp;</p>


                    </TD>
                    <TD width="250px" height="150" valign="top" align="left" id="FontPlain"
                        style="padding:25pt; background-color:white; border:1px solid gray">
                        <br />
                        <?php if (isset($profile_image)) : ?>
                        <div class="col-xs-12">
                            <img src="<?php echo $profile_image ?>" alt="..." class="img-thumbnail">

                        </div>
                        <?php else : ?>
                        <div class="col-xs-12">
                            <div class="alert alert-warning">
                                <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Please Upload Passport
                                ...
                                <?php if (isset($error)) {
                                        echo "<div class='alert alert-danger'>
                                        <span class='glyphicon glyphicon-info-sign'></span>
                                        <strong> $error ></strong> </div>";
                                    } ?>
                                <?php if (isset($successMSG)) {
                                        echo "<div class='alert alert-success'>
                                        <strong><span class='glyphicon glyphicon-info-sign'></span>$successMSG</strong> </div>";
                                    } ?>

                                <form method="post" enctype="multipart/form-data" class="form-horizontal">

                                    <table class="table table-bordered table-responsive">

                                        <tr>

                                            <td><input class="form-control" name='matric' id='matric'
                                                    value='<?php echo $_SESSION['matric']; ?>' type="hidden" /></td>
                                        </tr>



                                        <tr>
                                            <td><label class="control-label">Profile Image</label></td>
                                            <td><input class="input-group" type="file" name="user_image"
                                                    accept="image/*" />
                                            </td>
                                        </tr>
                                        <center>
                                            <tr>

                                                <td colspan="2"><button type="submit" name="btnsave"
                                                        class="btn btn-default">
                                                        <span class="glyphicon glyphicon-save"></span> &nbsp; SAVE
                                                    </button>
                                                </td>
                                            </tr>
                                        </center>

                                    </table>

                                </form>
                            </div>
                        </div>
                        <?php endif ?>



                        <hr />
                        <center>
                            <p><strong>Student Industrial Work Experience Scheme</strong>
                        </center>
                        <p align="justify">The Scheme exposes students to industry based skills necessary for a
                            smooth transition from the classroom to the world of work. It affords students of
                            tertiary institutions the opportunity of being familiarized and exposed to the needed
                            experience in handling machinery and equipment which are usually not available in the
                            educational institutions.
                        <p align="justify">Participation in SIWES has become a necessary pre-condition for the award
                            of Diploma and Degree certificates in specific disciplines in most institutions of
                            higher learning in the country, in accordance with the education policy of government.
                        <p align="justify">Operators - The ITF, the coordinating agencies (NUC, NCCE, NBTE),
                            employers of labour and the institutions.
                            Funding - The Federal Government of Nigeria
                            Beneficiaries - Undergraduate students of the following: Agriculture, Engineering,
                            Technology, Environmental, Science, Education, Medical Science and Pure and Applied
                            Sciences.
                            Duration - Four months for Polytechnics and Colleges of Education, and Six months for
                            the Universities.
                            <hr />
                            <center>
                                <?php if (loggedin()) {
                                    echo "<a href='required/logout.php'><button onclick='' class='submit' style='cursor:pointer; cursor:hand; padding:3pt'><b>Log Out</b></button></a></p>";
                                } else {
                                    echo "<p><a href='login.php'><button class='sumbit'
                                            style='cursor:pointer; cursor:hand; padding:3pt'><b>Log In</b></button></a></p>";
                                }
                                ?>

                            </center>
                            <hr />
                    </TD>
                </TR>
            </TABLE>
            <p>&nbsp;</p>
        </TD>
    </TR>
</TABLE>



<?php include("footer.php") ?>