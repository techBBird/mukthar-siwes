<?php include("main_page_handler.php"); ?>
<?php
include("topnav.php");
?>


<TABLE width="100%" cellpadding="0" cellspacing="0" border="0">
    <TR>
        <TD height="33" id="iPQLink" align="center">
            <div class="navi">
                <TABLE cellpadding="0" cellspacing="0" border="0" height="33">
                    <TR>

                        <TD width="114" align="center"><a href="mypage.php"
                                style="color:white; text-decoration:none">Dashboard</a></TD>
                        <TD width="10">|</TD>
                        <TD width="134" class="Hover" id="Link"><a href="logbook.php"
                                style="color:white; text-decoration:none">Fill Log Book</a></TD>
                        <TD width="10">|</TD>
                        <TD width="134" class="Hover" id="Link"><a href="mylogbooks.php"
                                style="color:white; text-decoration:none">Log Books</a></TD>
                        <TD width="10">|</TD>

                        <TD width="114" id="Link"><a href="search_job.php"
                                style="color:white; text-decoration:none">Search Job</a></TD>
                    </TR>
                </TABLE>
            </div>
        </TD>
    </TR>
</TABLE>


<!-- container -->
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?php if (isset($_GET['message'])) : ?>
            <div class="alert alert-success" id="message-alert" role="alert">
                <?php echo $_GET['message'] ?>
            </div>
            <?php endif ?>
            <?php if (isset($data_to_edit)) : ?>
            <form method="post">
                <h3>Edit Data</h3>
                <div class="form-row">
                    <div class="col">
                        <input type="hidden" class="form-control" name="matric"
                            value="<?php echo $data_to_edit['matric'] ?>">
                    </div>
                    <div class="col">
                        <input type="hidden" class="form-control" name="week"
                            value="<?php echo $data_to_edit['week'] ?>">
                    </div>
                    <div class="col">
                        <input type="hidden" class="form-control" name="data_to_edit"
                            value="<?php echo $data_to_edit['data_to_edit'] ?>">
                    </div>
                    <div class="col">
                        <input type="hidden" class="form-control" name="date_name"
                            value="<?php echo $data_to_edit['date_name'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="workdone">Data</label>

                    <input type="date" name="date" value="<?php echo $data_to_edit['date'] ?>" id="date">
                </div>
                <div class="form-group">
                    <label for="workdone">Data</label>

                    <textarea class="form-control" id="workdone" rows="3"
                        name="workdone"><?php echo $data_to_edit["workdone"] ?></textarea>
                </div>
                <button class="btn btn-lg btn-success" type="submit" name="edit">Save</button>
            </form>
            <?php endif ?>
            <?php if (!isset($data_to_edit)) : ?>
            <form class="form-p2" method="post">
                <div class="form-group">
                    <label for="week">Please Select Week</label>
                    <select class="form-control" id="week" name="week">
                        <option value="All"> All</option>
                        <?php foreach (range(1, 15) as $week) : ?>
                        <option value="<?php echo $week ?>"><?php echo $week ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-lg" name="view-logs"> Submit</button>
            </form>
            <?php endif ?>


            <?php if (isset($logbooks)) : ?>
            <?php $count = 0 ?>

            <?php foreach ($logbooks as $workdone) : ?>

            <?php $week = $workdone["week"];
                    unset($workdone["week"]); ?>


            <table class="table">
                <h3> Workdone for week <?php echo $week ?></h3>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Day</th>
                        <th scope="col">Date</th>
                        <th scope="col">Workdone</th>
                        <th scope="col"> Actions</th>
                    </tr>
                </thead>
                <?php foreach ($workdone  as $day => $data) : ?>

                <tbody>
                    <tr>
                        <th scope="row"><?php echo $week ?></th>
                        <td><?php echo $day ?></td>
                        <td><?php echo $data[1] ?></td>
                        <td><?php echo $data[0] ?></td>
                        <td>
                            <ul>
                                <li><a href="?action=edit&week=<?php echo $week ?>&day=<?php echo $day ?>">edit</a></li>
                                <li><a href="?action=delete&week=<?php echo $week ?>&day=<?php echo $day ?>">delete</a>
                                </li>
                            </ul>
                        </td>
                    </tr>

                </tbody>
                <?php endforeach ?>
                <?php endforeach ?>
            </table>


            <?php endif ?>
        </div>
        <div class="col-md-2"></div>

    </div>
</div>
<!-- /container -->



<?php include("footer.php") ?>