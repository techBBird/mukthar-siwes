<?php 
require 'required/utils.php';
require 'required/Database.php';
$session = Session::getInstance();
$user = $session->user

 ?>
<?php require 'topnav.php';?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <ul>
                <li> <a href="supervisor.php">Home</a></li>
                <li> <a href="viewlogbook.php">View Log Books</a></li>
            </ul>
        </div>
        <div class="col-md-10">
            <TABLE width="100%" cellpadding="0" cellspacing="0" border="0">


                <TR>
                    <TD height="364" align="center" style="padding-top:20pt">
                        <TABLE cellspacing="0" cellpadding="0">
                            <TR>
                                <TD width="870px" valign="top" id="FontPlain"
                                    style="padding-right:10px; background-color:white; border:1px solid gray">
                                    <p>&nbsp;</p>
                                    <?php echo"<span style='color:darkred; font-family:arial black; font-size:150%;'><b>&nbsp; Welcome, " . $user['initial'] . " " . $user['name'] . ".</b></span>" ?>
                                    <div class="text-center">
                                        <p><strong>Guidelines to check submitted log book</strong>
                                            <hr />
                                            <li>Enter student Matriculation Number</li>
                                            <br>
                                            <li>Choose the week you want to view</li>
                                            <br>
                                            <li>Click <b>VIEW to view the selected reports</b></li>
                                            <br>
                                            <li>Repeat the above steps to check for other week(s) or student(s)</li>

                                        </p>

                                    </div>

                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
            </TABLE>


        </div>

    </div>
</div>

<?php include("footer.php")?>