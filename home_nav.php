<header id="head">
    <div class="container">
        <div class="heading-text">
            <h1 class="animated flipInY delay1">Online SIWES Log Book</h1>
            <p>Fill your SIWES Log Book online.</p>
        </div>

        <div class="fluid_container">
            <div class="camera_wrap camera_emboss pattern_1" id="camera_wrap_4">
                <div data-thumb="assets/images/slides/thumbs/img1.jpg" data-src="assets/images/siii.jpg">
                    <h2>SIWES.</h2>
                </div>
                <div data-thumb="assets/images/slides/thumbs/img2.jpg" data-src="assets/images/sii.jpg">
                </div>
                <div data-thumb="assets/images/slides/thumbs/img3.jpg" data-src="assets/images/si.jpg">
                </div>
            </div><!-- #camera_wrap_3 -->
        </div><!-- .fluid_container -->
    </div>
</header>