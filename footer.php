<footer id="footer">

    <div class="container">
        <div class="row">
            <div class="footerbottom">
                <div class="col-md-3 col-sm-6">
                    <div class="footerwidget">
                        <h4>
                            Project Objectives
                        </h4>
                        <div class="menu-course">
                            <ul class="menu">
                                <li>Speed</li>
                                <li>Neat Report</li>
                                <li>Less Paper Work</li>
                                <li>Any time Response</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footerwidget">
                        <h4>
                            Why do this?
                        </h4>
                        <div class="menu-course">
                            <ul class="menu">
                                <li>Make SIWES easy</li>
                                <li>No loss of data</li>
                                <li>No Log Book loss</li>
                                <li>Available always</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footerwidget">
                        <h4>
                            Browse through pages
                        </h4>
                        <div class="menu-course">
                            <ul class="menu">
                                <li><a href="index.php">
                                        Home
                                    </a>
                                </li>
                                <li> <a href="register.php">
                                        Register
                                    </a>
                                </li>
                                <li><a href="login.php">
                                        Log In
                                    </a>
                                </li>
                                <li>
                                    <a href="about.php">
                                        About
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footerwidget">
                        <h4>Contact</h4>
                        <p>Obafemi Awolowo University Ile-Ife.</p>
                        <div class="contact-info">
                            <i class="fa fa-map-marker"></i> Osun State, Nigeria<br>
                            <i class="fa fa-phone"></i>+234 (0)8189065361<br>
                            <i class="fa fa-envelope-o"></i> murtadhobalogun@student.oauife.edu.ng
                        </div>
                    </div><!-- end widget -->
                </div>
            </div>
        </div>
        <div class="social text-center">
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-dribbble"></i></a>
            <a href="#"><i class="fa fa-flickr"></i></a>
            <a href="#"><i class="fa fa-github"></i></a>
        </div>

        <div class="clear"></div>
        <!--CLEAR FLOATS-->
    </div>
    <div class="footer2">
        <div class="container">
            <div class="row">

                <div class="col-md-6 panel">
                    <div class="panel-body">
                        <p class="simplenav">
                            <a href="index.php">Home</a> |
                            <a href="register.php">Register</a> |
                            <a href="login.php">Log In</a> |
                            <a href="about.php">About</a>

                        </p>
                    </div>
                </div>

                <div class="col-md-6 panel">
                    <div class="panel-body">
                        <p class="text-right">
                            Copyright &copy; 2021. Designed by <b>Balogun Murtadho Abiodun - CSC/2016/123.</b>
                        </p>
                    </div>
                </div>

            </div>
            <!-- /row of panels -->
        </div>
    </div>
</footer>

<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
<script src="assets/js/modernizr-latest.js"></script>
<script type='text/javascript' src='assets/js/jquery.min.js'></script>
<script type='text/javascript' src='assets/js/fancybox/jquery.fancybox.pack.js'></script>

<script type='text/javascript' src='assets/js/jquery.mobile.customized.min.js'></script>
<script type='text/javascript' src='assets/js/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='assets/js/camera.min.js'></script>
<script src="assets/js/bootstrap.min.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"
    integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css"
    integrity="sha512-8D+M+7Y6jVsEa7RD6Kv/Z7EImSpNpQllgaEIQAtqHcI0H6F4iZknRj0Nx1DCdB+TwBaS+702BGWYC0Ze2hpExQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.js"
    integrity="sha512-Y+cHVeYzi7pamIOGBwYHrynWWTKImI9G78i53+azDb1uPmU1Dz9/r2BLxGXWgOC7FhwAGsy3/9YpNYaoBy7Kzg=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css"
    integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />


<script src="assets/js/custom.js"></script>
<script>
jQuery(function() {

    jQuery('#camera_wrap_4').camera({
        transPeriod: 500,
        time: 3000,
        height: '600',
        loader: 'false',
        pagination: true,
        thumbnails: false,
        hover: false,
        playPause: false,
        navigation: false,
        opacityOnGrid: false,
        imagePath: 'assets/images/'
    });

});
</script>

<script>
var message = document.getElementById("message-alert")
if (message) {
    setTimeout(function() {
        message.style.display = "none"
    }, 2000)
}
</script>

</body>

</html>