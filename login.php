<?php $page_title = "LOGIN :: ONLINE SIWES LOG BOOK" ?>
<?php
require 'required/Database.php';
require 'required/utils.php';

use App\Database;

use function App\get_db;

$request_method = $_SERVER['REQUEST_METHOD'];


function is_empolyer_login()
{
    return request_is("POST") and isset($_POST["login_em"]);
}

function is_student_login()
{
    return request_is("POST") and isset($_POST["login_st"]);
}
if (is_empolyer_login()) {
    $data = escape_post_data();
    if (count($data) == 0) {
        redirect("login.php?type=em&error=<span style='color:red;'><b>Invalid Authentication credentials given!</b></span>");
    }

    $data = array_map("escape_input", $data);
    if (!exists($data, "email") || !validate_email($data['email'])) {
        redirect("login.php?type=em&error=<span style='color:red;'><b>Email not provided</b></span>");
    }
    if (!exists($data, "password")) {
        redirect("login.php?type=em&error=<span style='color:red;'><b>Password not provided</b></span>");
    }


    $email = $data['email'];
    $get_user = get_db()->select("select * from employers where email = '$email' ");
    if (is_empty_array($get_user)) {
        redirect("login.php?type=em&error=<span style='color:red;'><b>Email provided not found!</b></span>");
    }

    $user = $get_user[0];
    $password_hash = $user['password'];
    if (!password_verify($data['password'], $password_hash)) {
        redirect("login.php?type=em&error=<span style='color:red;'><b>Password or Email not Valid!</b></span>");
    }

    if (password_needs_rehash($password_hash, PASSWORD_BCRYPT)) {
        $password_hash = password_hash($data['password'], PASSWORD_BCRYPT);
    }
    $session = Session::getInstance();
    $session->user = $user;
    $session->isEmployer = true;
    $session->isStudent = false;
    $session->isSupervisor = false;
    redirect("employers.php");
    // var_dump($user);



}

if (is_student_login()) {
    if (empty($_POST['matric']) || !isset($_POST['matric'])) {
        $matric_error = "<span style='color:red;'><b>Please Enter Your Log In Details!</b></span>";
    } else {
        $matric = $_POST['matric'];
        $password = $_POST['password'];
        $login = login_user(get_db(), $matric, $password);
        if ($login) {
            redirect("mypage.php");
            // echo "login successful";
        } else {
            echo "<span style='color:red;'><b>Invalid Authentication credentials given!</b></span>";
        }
    }
}
?>

<?php

if ($request_method == "GET") {
    $type = $_GET['type'] ?? null;

    if (isset($type) && $type == "em") {

        $display_em = true;
    } elseif (isset($type) && $type = "student") {
        $display_st = true;
    } elseif (isset($type) && $type == "super") {
        $display_sp = true;
    }
}
?>

<?php include("topnav.php") ?>



<header id="head" class="secondary">
    <div class="container">
        <h1>LOG IN
            <?php if (isset($display_em)) : ?> As Employer <?php endif ?></php>
        </h1>
        <p>Log In with your Username!</p>
    </div>
</header>


<!-- container -->
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?php if (!isset($display_em)) : ?>
            <h3 class="section-title">How to Log In</h3>
            <p>Your Matriculation is your username</p>
            <?php endif ?>
            <?php
            if (isset($display_em) && $display_em == true) {
                // echo "display employe";
                include("employers_login_form.php");
            } elseif (isset($display_sp) and $display_sp) {
                include("supervisorlogin.php");
            } else {
                if (isset($matric_error)) echo $matric_error;
                include("login_form.php");
            }
            ?>
        </div>
        <div class="col-md-2"></div>

    </div>
</div>
<!-- /container -->
<?php include("footer.php"); ?>