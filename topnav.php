<!DOCTYPE html>
<html lang="en">
<?php
$default_title = $page_title ?? "HOME :: ONLINE SIWES LOG BOOK" ;
?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="free-educational-responsive-web-template-webEdu">
    <meta name="author" content="webThemez.com">
    <title><?php echo $default_title?></title>
    <link rel="favicon" href="assets/images/favicon.png">
    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel='stylesheet' id='camera-css' href='assets/css/camera.css' type='text/css' media='all'>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>


    <?php if(isset($main_page)){
        echo "<link type='text/css' href='assets/css/home.css' rel='stylesheet' />";
    }
    ?>

</head>

<body>

    <div class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <!-- Button for smallest screens -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <a class="navbar-brand" href="index.php">

                    <img src="assets/images/logooo.png" alt="Techro HTML5 template"></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav pull-right mainNav">
                    <li class="active"><a href="index.php">Home</a></li>
                    <?php if(!loggedin()){
                        
                        echo "<li><a href='logIn.php'>Log In</a></li>";

                    }else{
                        echo  "<li><a href='required/logout.php'>Log Out</a></li>";
                    }
                    if(!isset($is_register_page) || !$is_register_page){
                        echo "<li><a href='register.php'>Register</a></li>";
                        
                    }
                   
                    
                    ?>

                    <li><a href="about.php">About</a></li>

                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>


    <?php 
    if(isset($show_home_nav) && $show_home_nav == true) {
        include("home_nav.php");
    }
    
    ?>