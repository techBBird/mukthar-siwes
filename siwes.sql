-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 12, 2021 at 04:17 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siwes`
--

-- --------------------------------------------------------

--
-- Table structure for table `employers`
--

CREATE TABLE `employers` (
  `id` bigint(20) NOT NULL,
  `fname` text NOT NULL,
  `cname` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `industry` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employers`
--

INSERT INTO `employers` (`id`, `fname`, `cname`, `email`, `password`, `industry`, `created_at`, `updated_at`) VALUES
(1, 'Yusuf Adegoke', 'Company name', 'test@testmail.com', '$2y$10$uT2pjix4G3Jw21r1QMNXcuEQIvOozUFNTMmc3wqL5QWVmYRd7J3ga', 'Information Technology', '2021-12-04 23:36:05', NULL),
(2, 'yusuf', 'adegoke', 'email@mail.com', '$2y$10$bz3oclmsx2SgutLvhCtDCOQ9qgO/jYvJ/XFXfONQPhyvaf3H5b1Qy', 'Architecture', '2021-12-05 01:21:31', NULL),
(3, 'yusuf', 'adegoke', 'email1@mail.com', '$2y$10$wB9X4aUKULLYkBevn3v9aObS5A2nvlbTNCC8awtKy15YDJo9rFRKe', 'Engineering', '2021-12-05 01:22:47', NULL),
(4, 'Balogun Abiodun', 'Abbey Nigeria Ltd', 'abbey@gmail.com', '$2y$10$tOwds15VbaUspkbHN7/.I.ArL.dt7Kpbh8UaDZOXHZYDZljIzjpBC', 'Information Technology', '2021-12-06 10:10:31', NULL),
(5, 'Adegbite Yusuf Adegoke', 'Heritage Technology', 'ade@mail.com', '$2y$10$VZrimYFs0qrs8REIcEmHEehtRjkkOPvO9fGaqPUtX8TKWAffOGUJ.', 'Architecture', '2021-12-12 15:17:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_postings`
--

CREATE TABLE `job_postings` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `title` text NOT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `location` text DEFAULT NULL,
  `description` text NOT NULL,
  `requirements` text NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `number_of_openings` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `closed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_postings`
--

INSERT INTO `job_postings` (`id`, `user_id`, `title`, `tag`, `location`, `description`, `requirements`, `paid`, `number_of_openings`, `created_at`, `closed_at`) VALUES
(1, 3, 'testing', 'Testing', NULL, '&lt;p&gt;Testing&lt;/p&gt;', '&lt;p&gt;testing&lt;/p&gt;', 1, 2, '2021-12-06 03:51:28', '2021-12-22 03:51:28'),
(2, 3, 'testing', 'software development', NULL, '&lt;p&gt;Testing&lt;/p&gt;', 'The main idea of hierarchical clustering is based on the concept that nearby objects are more related than objects that are farther away. Let’s take a closer look at various aspects of these algorithms:\n\nThe algorithms connect to “objects” to form “clusters” based on their distance.\nA cluster can be defined by the max distance needed to connect to the parts of the cluster.\nDendrograms can represent different clusters formed at different distances, explaining where the name “hierarchical clustering” comes from. These algorithms provide a hierarchy of clusters that at certain distances are merged.\nIn the dendrogram, the y-axis marks the distance at which clusters merge. The objects are placed beside the x-axis such that clusters don’t mix.\nHierarchical clustering is a family of methods that compute distance in different ways. Popular choices are known as single-linkage clustering, complete linkage clustering, and UPGMA. Furthermore, hierarchical clustering can be:', 1, 23, '2021-12-06 10:04:12', '2021-12-31 10:04:12');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `ID` int(255) NOT NULL,
  `week` varchar(4) DEFAULT NULL,
  `matric` varchar(32) DEFAULT NULL,
  `monday` varchar(20) DEFAULT NULL,
  `mondate` date DEFAULT NULL,
  `mondata` text DEFAULT NULL,
  `tuesday` varchar(20) DEFAULT NULL,
  `tuesdate` date DEFAULT NULL,
  `tuesdata` text DEFAULT NULL,
  `wednesday` varchar(20) DEFAULT NULL,
  `wednesdate` date DEFAULT NULL,
  `wednesdata` text DEFAULT NULL,
  `thursday` varchar(20) DEFAULT NULL,
  `thursdate` date DEFAULT NULL,
  `thursdata` text DEFAULT NULL,
  `friday` varchar(20) DEFAULT NULL,
  `fridate` date DEFAULT NULL,
  `fridata` text DEFAULT NULL,
  `saturday` varchar(20) DEFAULT NULL,
  `saturdate` date DEFAULT NULL,
  `saturdata` text DEFAULT NULL,
  `grade` int(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`ID`, `week`, `matric`, `monday`, `mondate`, `mondata`, `tuesday`, `tuesdate`, `tuesdata`, `wednesday`, `wednesdate`, `wednesdata`, `thursday`, `thursdate`, `thursdata`, `friday`, `fridate`, `fridata`, `saturday`, `saturdate`, `saturdata`, `grade`) VALUES
(3, '1', '1530110002', 'Monday', '2017-08-28', 'I worked', 'Tuesday', '2017-08-29', 'I worked mmg', 'Wednesday', '2017-08-30', 'I worked bbr', 'Thursday', '2017-08-31', 'I worked ffo1', 'Friday', '2017-09-01', 'I worked 20 minutes', 'Saturday', '2017-09-02', 'I worked everyday', NULL),
(4, '2', '1530110002', 'Monday', '2017-09-04', 'We worked tirelessly', 'Tuesday', '2017-09-05', 'We worked tirelessly more', 'Wednesday', '2017-09-06', 'We worked tirelessly more...', 'Thursday', '2017-09-07', 'We worked tirelessly more......', 'Friday', '2017-09-08', 'We worked tirelessly more...........', 'Saturday', '2017-09-09', 'We worked tirelessly more...... more', NULL),
(5, '1', '32243243', 'Monday', '2017-09-11', 'iii', 'Tuesday', '2017-09-12', 'We worked tirelessly more...... bdvhcy', 'Wednesday', '2017-09-13', 'We worked tirelessly more......uguyfc', 'Thursday', '2017-09-14', 'mhgjcy We worked tirelessly more......', 'Friday', '2017-09-15', ' sjgaycgfkvlyvkcvloyfdclkuvckuyflkyu', 'Saturday', '2017-09-16', 'vkaydufckahc nalci7dgci', NULL),
(6, '2', '32243243', 'Monday', '2017-09-18', 'kjmhdlekblidhcWe worked tirelessly more......We worked tirelessly more......', 'Tuesday', '2017-09-19', 'hgjyuscvkuhevclivdicuWe worked tirelessly more......We worked tirelessly more......', 'Wednesday', '2017-09-20', 'hgacyugvlke,jcbogdeickjWe worked tirelessly more......We worked tirelessly more......We worked tirelessly more......', 'Thursday', '2017-09-21', 'uihisugoicfo8ysfckhwvco', 'Friday', '2017-09-22', 'Âµhkvk We worked tirelessly more......We worked tirelessly more......We worked tirelessly more......', 'Saturday', '2017-09-23', 'kyyfhnhWe worked tirelessly more......nyuguyocu7kcyWe worked tirelessly more......mjyafiyuf', NULL),
(7, '1', 'csc/2015/007', 'Monday', '2021-10-19', 'fucking edit this data and the date', 'Tuesday', '2021-12-11', 'This is Tuesday data', NULL, NULL, NULL, NULL, NULL, NULL, 'Friday', NULL, NULL, 'Saturday', NULL, NULL, 3),
(8, '2', 'csc/2015/007', 'Monday', '2021-11-01', 'hgacyugvlke,jcbogdeickjWe worked tirelessly more......We worked tirelessly more......We worked tirelessly more......', 'Tuesday', NULL, NULL, 'Wednesday', NULL, NULL, 'Thursday', NULL, NULL, 'Friday', NULL, NULL, 'Saturday', NULL, NULL, NULL),
(12, '3', 'csc/2015/007', 'Monday', '2021-12-06', 'job description here', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '1', 'CSC/2016/123', 'Monday', '2021-12-13', '<p>I was introduced to the concept of networking.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `ID` int(11) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `matric` varchar(25) NOT NULL,
  `password` text NOT NULL,
  `gender` varchar(11) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `phone1` varchar(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `supervisor_id` varchar(255) NOT NULL,
  `level` varchar(50) NOT NULL,
  `cname` text NOT NULL,
  `caddress` varchar(2000) NOT NULL,
  `cphone` varchar(11) NOT NULL,
  `cemail` varchar(100) NOT NULL,
  `csname` varchar(170) NOT NULL,
  `csphone` varchar(11) NOT NULL,
  `csemail` varchar(102) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`ID`, `surname`, `firstname`, `middlename`, `matric`, `password`, `gender`, `phone`, `phone1`, `email`, `department`, `supervisor_id`, `level`, `cname`, `caddress`, `cphone`, `cemail`, `csname`, `csphone`, `csemail`) VALUES
(5, 'Balogun', 'Murtadho', 'Abiodun', 'CSC/2016/123', '$2y$10$dcbCMyWntGfTr7d4oT8XnulTJM3ZG2KuFPvfvdCYIySYMIEbzB0Ya', 'Male', '08189065361', '08105552506', 'Murtadhob@yahoo.com', 'Computer Science and Engineering', '1234567890', 'part 4', 'Crown Hope Computer School', 'Apata, Ibadan.', '08155893147', 'chcs@yahoo.com', 'Mr Adebayo Adedayo', '08138334733', 'adebayo21@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `supervisors`
--

CREATE TABLE `supervisors` (
  `id` int(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `initial` varchar(45) NOT NULL,
  `name` varchar(400) NOT NULL,
  `department` text DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `school` text DEFAULT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supervisors`
--

INSERT INTO `supervisors` (`id`, `username`, `initial`, `name`, `department`, `phone`, `email`, `password`, `school`, `created_at`) VALUES
(3, '122334543', 'Prof.', 'H.A Soriyan', NULL, '08037179520', 'hasoriyan@gmail.com', NULL, NULL, '2021-12-10'),
(4, 'adegoke1234', 'Mr.', 'Yusuf adegoke', 'Accountancy', '090123467898', 'mail@supervisormail.com', '$2y$10$Za/o2qT.4WzXH7cf9HZ37.W7MtcKv8tp2u3qcB1EU5OgTfd9QDLAC', 'Obafemi Awolowo university', '2021-12-10'),
(5, 'gokeobasa', 'Mr', 'Goke Obasa', 'Business Administration', '090898878998', 'gokeobase@gmail.com', '$2y$10$.dlvSXQukuZ/QePcFk7LNeLhXXSvWP9Z/otmekvIfYc315j/KB8Ny', 'Obafemi awolowo university', '2021-12-10'),
(6, 'Ayeni', 'Mr', 'Ayeni', 'Computer Science and Engineering', '08189065361', 'Ayeni21@gmail.com', '$2y$10$oxW40eh6dw1/NHhHjZseH.z0dAey5R6R93C5V2xG3cVjGkteQX9AS', 'Obafemi Awolowo University, Ile Ife', '2021-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userID` int(11) NOT NULL,
  `matric` varchar(20) NOT NULL,
  `userPic` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userID`, `matric`, `userPic`) VALUES
(66, 'CSC/2016/123', '481259.jpeg'),
(65, 'csc/2015/007', '189924.png'),
(64, 'csc/2015/007', '915806.png'),
(63, 'csc/2015/007', '196087.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employers`
--
ALTER TABLE `employers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING HASH;

--
-- Indexes for table `job_postings`
--
ALTER TABLE `job_postings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `supervisors`
--
ALTER TABLE `supervisors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_user_name` (`username`) USING HASH;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employers`
--
ALTER TABLE `employers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `job_postings`
--
ALTER TABLE `job_postings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `supervisors`
--
ALTER TABLE `supervisors`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
