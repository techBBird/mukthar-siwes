<?php

namespace App;

if (PHP_OS == "Darwin") {
    define('DB_HOST', "localhost:3306");
    define('DB_NAME', "siwes");
    define('DB_USERNAME', "root");
    define("DB_PASSWORD", '');
} else {
    define('DB_HOST', "sql207.epizy.com");
    define('DB_NAME', "epiz_30224305_siwes");
    define('DB_USERNAME', "epiz_30224305");
    define("DB_PASSWORD", 'BgmeXaEFW3rw');
}

class Database extends \PDO
{

    function __construct($username = '', $password = '', $dbName = '', $host = DB_HOST)
    {

        try {
            if ($dbName === '') {
                $dbName = DB_NAME;
                $username = DB_USERNAME;
                $password = DB_PASSWORD;
                $host = DB_HOST;
            }
            parent::__construct("mysql:host=" . $host . ";dbname=$dbName", "$username", "$password");
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }




    function select($query, $array = array())
    {
        $con = $this->prepare($query);
        //        print_r($con);


        foreach ($array as $key => $value) {
            $con->bindValue($key, $value);
        }

        $con->execute();
        return $con->fetchAll(\PDO::FETCH_ASSOC);
    }
    function _select($sql)
    {
        $stmt = $this->prepare("SELECT * FROM `$sql`");
        $stmt->execute();
        $rs = $stmt->fetchAll();
        return $rs;
    }

    public function update($table, $data, $where)
    {
        ksort($data);
        $fieldDetails = NULL;
        foreach ($data as $key => $value) {
            $fieldDetails .= "`$key`=:$key,";
        }
        $fieldDetails = rtrim($fieldDetails, ',');
        //        print("UPDATE $table SET $fieldDetails WHERE $where");
        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
            //            print($value);
        }
        if ($sth->execute()) {
            return $id = $this->select("select * from $table WHERE " . $where . " order by id DESC LIMIT 1 ");
        } else {
            return FALSE;
        }
    }

    public function delete($table, $where, $limit = 1)
    {
        $sql = "DELETE FROM $table WHERE $where LIMIT $limit";
        // print_r($sql);
        return $this->exec($sql);
    }


    function insert($table, $array)
    {

        $key = array_keys($array);

        $ope = implode("`,`", $key);
        $yemi = "`" . $ope . "`";
        $ola = implode(",:", $key);
        $mide = ":" . $ola;
        $wuraola = "insert into $table ($yemi) values($mide)";
        $con = $this->prepare($wuraola);
        foreach ($array as $key => $value) {
            $con->bindValue($key, $value);
        }
        if ($con->execute()) {
            return  $this->select("select * from $table order by id DESC LIMIT 1 ");
        }
        return FALSE;
    }



    function e($q)
    {
        return $this->exec($q);
    }
}

if (!function_exists("get_connection")) {

    function get_connection()
    {

        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        return $conn;
    }
}
if (!function_exists("get_db")) {

    function get_db()
    {
        return new Database();
    }
}