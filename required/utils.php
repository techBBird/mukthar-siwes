<?php

use App\Database;
// use App\function\get_connection;

use function App\get_connection;
use function App\get_db;

function escape_input($value)
{
    return htmlentities($value, ENT_QUOTES, 'UTF-8');
}


/**
 * check if an array null or empty
 * @param array $value
 * @return bool
 */
function is_empty_array(array $value)
{
    return array_length($value) == 0 || is_null($value) || empty($value);
}


function is_associative(array $array)
{
    return array_keys($array) !== range(0, array_length($array) - 1);
}


function confirmAllPresent(array $expected, array $actual)
{
    $provided_fields = array_uintersect($expected, $actual, "strnatcasecmp");
    $unprovided_fields = array_diff($expected, $provided_fields);
    if (!empty($unprovided_fields)) {
        return $unprovided_fields;
    }
    return true;
}
function array_length(array $array)
{
    return count($array);
}
function quotes($value)
{
    if (is_array($value)) {
        return implode(",", array_map(__FUNCTION__, $value));
    }
    return  "'$value'";
}

function redirect($url)
{
    if (headers_sent()) {
        die('<script type="text/javascript">window.location.href="' . $url . '";</script>');
    } else {
        header('Location: ' . $url);
        die();
    }
}


$current_file = $_SERVER['SCRIPT_NAME'];

function loggedin()
{
    if (isset($_SESSION['user'])) {
        return true;
    } else {
        return false;
    }
}

function getstudentsfield($field)
{
    global $conn;
    $query = "SELECT $field FROM students WHERE id='" . $_SESSION['user_id'] . "'";
    if ($query_run = mysqli_query($conn, $query) !== false) {
        return $query_run;
    }
}

function register_user(array $data, Database $connect)
{
    try {
        $fields_required = [
            'surname', 'firstname', 'middlename', 'matric', 'gender', 'phone', 'phone1', 'email', 'department', 'level',
            'cname', 'caddress', 'cphone', 'cemail', 'csname', 'csphone', "supervisor_id", "password"
        ];
        $data_not_provided = check_all_fields_present($data, $fields_required);
        // print_r($data_not_provided);
        if (is_array($data_not_provided)) {
            $data_not_provided = quotes($data_not_provided);
            // echo $data_not_provided;

            return "<p><span style='color:red;'><b>error, the following fields are not provided: $data_not_provided</b></span></p><br />";
        }
        $post_data = array_filter(array_map(function ($key) {
            return escape_input($key) ?? null;
        }, $data));



        $matric = $post_data['matric'];
        $has_registered_before = $connect->select("SELECT matric FROM students WHERE matric='$matric'");
        if ($has_registered_before) {
            return "<b><span style='color:blue;'  face:'courier'>The matriculation number '<span style='color:red;'  face:'courier'>" . $matric . "</span>' already Exists</span></b>";
        }
        $post_data['password'] = password_hash($post_data['password'], PASSWORD_BCRYPT);
        $register = $connect->insert("students", $post_data);
        if ($register) {
            return [true, false];
        }
        return  [false, "<p><span style='color:red;'><b>Please Try Again</b></span></p><br />"];
    } catch (Exception $e) {
        return [false,$e->getMessage()];
    }
}

function render_user_details($user, $supervisor)
{

    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Name: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['surname'] . " " . $user['firstname'] . " " . $user['middlename'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Matriculation Number: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['matric'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Gender: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['gender'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Phone Number: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['phone'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Alternate Phone Number: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['phone1'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Email: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['email'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Department: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['department'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Level: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['level'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Company Name: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['cname'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Company Address: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['caddress'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Company Phone Number: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['cphone'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Company Email: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['cemail'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Company Supervisor Name: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['csname'] . "<br /> </font> </p>";
    echo "<p><hr color=pink></p>";
    echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; Company Supervisor Phone: </font> <font color=darkblue font face='courier' size='4pt'>" . $user['csphone'] . "<br /> </font> </p>";

    echo "<p><hr color=pink></p>";
    if (!empty($supervisor)) {
        echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; School Supervisor Name: </font> <font color=darkblue font face='courier' size='4pt'>" . $supervisor['initial'] . " " . $supervisor['name'] . "<br /> </font> </p>";
        echo "<p><hr color=pink></p>";
        echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; School Supervisor Phone: </font> <font color=darkblue font face='courier' size='4pt'>" . $supervisor['phone'] . "<br /> </font> </p>";
        echo "<p><hr color=pink></p>";
        echo "<p> <font color=black font face='courier' size='4pt'>&nbsp; School Supervisor Email: </font> <font color=darkblue font face='courier' size='4pt'>" . $supervisor['email'] . "<br /> </font> </p>";
    }


    echo "<p><hr color=pink></p>";
}
function login_user(Database $conn, $matric, $password)
{

    $query = "SELECT * FROM students WHERE matric='$matric'";
    $user = $conn->select($query);
    if ($user) {
        // ob_start();
        $user = $user[0];
        $password_is_valid = password_verify($password, $user['password']);
        if ($password_is_valid) {
            if (password_needs_rehash($user['password'], PASSWORD_BCRYPT)) {
                $password = password_hash($password, PASSWORD_BCRYPT);
                $data = ['password' => $password];
                $conn->update('supervisor', $data, "matric='$matric");
            }
            $session = Session::getInstance();
            $session->user = $user;
            $session->isStudent = true;
            return true;
        }
        return false;
    }
    return false;
}

function save_image(Database $conn, $matric, $userpic)
{
    $stmt = $conn->prepare('INSERT INTO tbl_users(matric,userPic) VALUES(:matric, :upic)');
    $stmt->bindParam(':matric', $matric);
    $stmt->bindParam(':upic', $userpic);

    if ($stmt->execute()) {
        return true;
        // header("refresh:5;mypage.php"); // redirects image view page after 5 seconds.
    }
    return false;
}

function check_all_fields_present(array $input, array $required_keys)
{
    if (!is_associative($input) || is_associative($required_keys)) {
        return false;
    }
    $actual = array_keys(array_filter($input));
    if (array_length($actual) == 0) {
        return false;
    }
    return confirmAllPresent($required_keys, $actual);
}

function exists($array, $key): bool
{
    if (is_empty_array($array)) {
        return false;
    }
    if ($array instanceof ArrayAccess) {
        return $array->offsetExists($key);
    }
    return array_key_exists($key, $array);
}
function escape_post_data()
{
    $data = array_filter(array_map(function ($input) {
        return mysqli_escape_string(get_connection(), $input);
    }, $_POST), function ($input) {
        return !is_null($input) && (is_string($input) ? $input != '' : !empty($input));
    });
    return $data;
}
function get_request_data()
{
    $data = escape_post_data();
    if (count($data) > 0) {
        $data =  array_map("escape_input", $data);

        $error = [];
        if (!isset($data['fname'])) {
            $error["fname"] = true;
        }
        echo $data['password'];
        if (!isset($data['password']) || strlen($data['password']) < 8) {
            $error['password'] = true;
        }
        if (!isset($data['cname'])) {
            $error["cname"] = true;
        }
        if (!isset($data['email']) || !validate_email($data['email'])) {
            $error['email'] = true;
        }
        if ($error) {
            return [false, $error];
        }

        return [$data, false];
    } else {
        return [false, true];
    }
}
function validate_email($value)
{

    if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
        return true;
    } else {
        return false;
    }
}

function register_employee($data)
{

    try {
        $db = get_db();
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        $email = $data['email'];
        $user_exists = $db->select("select email from employers where email= '$email'");
        if (count($user_exists) > 0) {
            return [false, "user email already exists"];
        }

        $inserted = $db->insert("employers", $data);
        if ($inserted) {
            return [$inserted, false];
        }
        return [false, "error while creating user, try again latter"];
    } catch (\PDOException $e) {
        $error = $e->getMessage();
        return [false, $error];
    }
}



if (!function_exists("request_is")) {
    function request_is(string $type)
    {
        return $_SERVER['REQUEST_METHOD'] == $type;
    }
}

if (!function_exists("is_supervisor")) {
    function is_supervisor()
    {
        return Session::getInstance()->isSupervisor;
    }
}

if (!function_exists("is_employer")) {
    function is_employer()
    {
        return Session::getInstance()->isEmployer;
    }
}

if (!function_exists("is_student")) {
    function is_a_student()
    {
        return !is_null(Session::getInstance()->user) &&
            Session::getInstance()->isStudent;
    }
}

if (!function_exists("can_view_log_book")) {
    function can_view_log_book()
    {
        return is_supervisor() || is_employer() || is_a_student();
    }
}



class Session
{
    const SESSION_STARTED = TRUE;
    const SESSION_NOT_STARTED = FALSE;

    // The state of the session
    private $sessionState = self::SESSION_NOT_STARTED;

    // THE only instance of the class
    private static $instance;


    private function __construct()
    {
    }


    /**
     *    Returns THE instance of 'Session'.
     *    The session is automatically initialized if it wasn't.
     *    
     *    @return    object
     **/

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }

        self::$instance->startSession();

        return self::$instance;
    }


    /**
     *    (Re)starts the session.
     *    
     *    @return    bool    TRUE if the session has been initialized, else FALSE.
     **/

    public function startSession()
    {
        if ($this->sessionState == self::SESSION_NOT_STARTED) {
            $this->sessionState = session_start();
        }

        return $this->sessionState;
    }


    /**
     *    Stores datas in the session.
     *    Example: $instance->foo = 'bar';
     *    
     *    @param    name    Name of the datas.
     *    @param    value    Your datas.
     *    @return    void
     **/

    public function __set($name, $value)
    {
        $_SESSION[$name] = $value;
    }


    /**
     *    Gets datas from the session.
     *    Example: echo $instance->foo;
     *    
     *    @param    name    Name of the datas to get.
     *    @return    mixed    Datas stored in session.
     **/

    public function __get($name)
    {
        if (isset($_SESSION[$name])) {
            return $_SESSION[$name];
        }
    }


    public function __isset($name)
    {
        return isset($_SESSION[$name]);
    }


    public function __unset($name)
    {
        unset($_SESSION[$name]);
    }


    /**
     *    Destroys the current session.
     *    
     *    @return    bool    TRUE is session has been deleted, else FALSE.
     **/

    public function destroy()
    {
        if ($this->sessionState == self::SESSION_STARTED) {
            $this->sessionState = !session_destroy();
            unset($_SESSION);

            return !$this->sessionState;
        }

        return FALSE;
    }
}