<?php

use function App\get_db;

include("required/utils.php");
include("required/Database.php");
$is_register_page  = true;
include("topnav.php");


$session = Session::getInstance();

if (!loggedin() || !$session->isEmployer) {
    redirect("login.php?type=em");
}

$user = $session->user;

if (request_is("GET")) {
    if (isset($_GET['message'])) {
        $message = $_GET['message'];
    }
}


?>

<header id="head" class="secondary">
    <div class="container">
        <h1>Welcome Back</h1>
        <p><?php echo $user['fname'] ?></p>
    </div>
</header>

<div class="container">

    <div class="row register-menu">
        <div class="col-md-3">

            <ul class="list-group">
                <li class="list-group-item"> <a href="create_job.php">Create Job Posting</a></li>
                <li class="list-group-item"><a href="viewlogbook.php">View Student Log book</a></li>
                <li class="list-group-item"><a href="viewapplicants.php">View Job Applications</a></li>

            </ul>


        </div>
        <div class="col-md-9">

            <?php if (isset($message)) : ?>
            <div class="alert alert-success" role="alert" id="message-alert">
                <?php echo $message ?>
            </div>

            <?php endif ?>

            <?php
            $user_id = $user['id'];
            $jobs = get_db()->select("select * from job_postings where user_id=:user_id", ['user_id' => $user_id]);
            ?>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Date Created</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($jobs as $job) : ?>
                    <tr>
                        <th scope="row"><?php echo $job['id'] ?></th>
                        <td><?php echo $job['title'] ?></td>
                        <td> <?php echo $job['description'] ?></td>
                        <td><?php echo $job['created_at'] ?></td>
                        <td>

                            <ul class="btn-group" role="group" aria-label="jobs-actions">
                                <li class="btn btn-success"><a style="color:white; text-decoration:none"
                                        href="#">Edit</a></li>
                                <li class="btn btn-success"> <a style="color:white; text-decoration:none" href="#">View
                                        Job Details</a></li>
                                <li class="btn btn-danger"><a style="color:white; text-decoration:none"
                                        href="#">Delete</a></li>
                            </ul>


                        </td>
                    </tr>
                    <?php endforeach ?>


                </tbody>
            </table>


        </div>

    </div>

</div>

<?php include("footer.php") ?>