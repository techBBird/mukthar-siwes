<?php

use App\Database;

include("required/Database.php");

$db =  new Database();

$matric = $_POST['matric'];
$week = $_POST['week'];

$sql = "SELECT * FROM reports where matric='$matric' and week='$week'";
$result = $db->select($sql);
if ($result) {
    $detail = $result[0];

    echo "<b>" . $detail['matric'] . "'S WEEK " . $detail['week'] . " SIWES LOG BOOK REPORTS</b>";
} else {
    echo "Invalid Details";
}

?>
<br />
<?php

function render_report($week, $day, $workdone)
{
    echo <<<EOT

        <div class='col-md-3'>

          <div class='card' style='width: 18rem;'>
         <div class='card-body'>
        <h5 class="card-title"> WEEK $week</h5>
        <h6 class="card-subtitle mb-2 text-muted">$day</h6>
        <p class="card-text">
            "$workdone"
        </p>
        <a href="#" class="card-link">Approve</a>
        <a href="#" class="card-link">Add Comment</a>
        </div>
        </div>
        </div>
  
   
     
EOT;
}
?>


<?php
if (isset($detail)) {
    $row = $result[0];
    $data = [
        "Monday" => $row['mondata'],
        "Tuesday" => $row['tuesdata'],
        "Wednessday" => $row['wednesdata'],
        "Thursday" => $row['thursdata'],
        "Friday" => $row["fridata"],
        "Saturdata" => $row['saturdata']
    ];

    echo "<div class ='container' >";
    echo "<div class='row'>";
    foreach ($data as $day => $workdone) {
        render_report($week, $day, $workdone);
    }
    echo " </div> </div>";
}

?>