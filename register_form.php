<div class="container">

    <div class="row register-menu">
        <div class="col-md-3">
            <nav id="sidebar">
                <ul class="list-unstyled components">

                    <li class="register-menu">
                        <a class="btn" href="register_employer.php">Register As Employer</a>
                    </li>

                    <li class="register-menu">
                        <a class="btn" href="register_supervisor.php">Register as a Supervisor</a>
                    </li>

                </ul>
            </nav>


        </div>
        <div class="col-md-9">

            <form action="register.php" method="post" role="form">

                <div class="row">
                    <h3 class="section-title ">Personal Information</h3>
                    <span>
                        <?php if (isset($_GET['error'])) : ?>
                            <p><span class="text-danger"><b><?php echo $_GET['error'] ?></b></span></p><br />
                        <?php endif ?>

                    </span>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Surname</label>
                            <input type="text" name="surname" class="form-control" placeholder="Surname" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Firstname</label>
                            <input type="text" name="firstname" class="form-control" placeholder="Firstname" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Middlename</label>
                            <input type="text" name="middlename" class="form-control" placeholder="Middlename" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Matriculation Number</label>
                            <input type="text" name="matric" class="form-control" placeholder="Matriculation Number" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Enter your password" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Gender</label>
                            <select name="gender" class="form-control" required>
                                <option value="">Select Gender</option>
                                <option value="Female">Female</option>
                                <option value="Male">Male</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone" class="form-control" placeholder="Phone Number eg. 08012345678" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Alternate Phone Number</label>
                            <input type="text" name="phone1" class="form-control" placeholder="Alternate Phone Number eg. 08012345678" required>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Email eg. myname@email.com" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Department</label>
                            <select name="department" class="form-control" required>
                                <option value="">Select Department</option>
                                <?php foreach ([
                                    "Computer Science and Engineering",
                                    "Chemical Engineering", "Civil Engineering",
                                    "Electronics and Electrical Engineering", "Food Science and Technology",
                                    "Mechanical Engineering", "Material Science and Engineering",
                                    "Agricultural and Environmental Engineering", "Building", "Architecture",
                                    "Estate Management", "Quantity Surveying", "Fine and Applied Arts", "Urban and Regional Planning",
                                    "Zoology", "Botany", "Microbiology", "Chemistry", "BioChemistry"
                                ] as $department) : ?>

                                    <option value="<?php echo $department ?>"><?php echo $department ?></option>
                                <?php endforeach ?>

                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Level</label>
                            <select name="level" class="form-control" required>
                                <option value="">Select Level</option>
                                <option value="part 3">PART 3</option>
                                <option value="part 4">PART 4</option>
                            </select>
                        </div>
                    </div>
                </div>

                <h3 class="section-title">Placement Information</h3>



                <div class="form-group">
                    <label>Company Name</label>
                    <input type="text" name="cname" class="form-control" placeholder="Company name" required>
                </div>
                <div class="form-group">
                    <label>Company Address</label>
                    <textarea name="caddress" class="form-control" id="message" placeholder="Company Address..." required style="height:100px;"></textarea>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Company Phone Number</label>
                            <input type="text" name="cphone" class="form-control" placeholder="Company Phone Number eg. 08012345678" required>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Company Email</label>
                            <input type="email" name="cemail" class="form-control" placeholder="Company Email eg. companyname@email.com" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Company Supervisor Name</label>
                            <input type="text" name="csname" class="form-control" placeholder="Company Supervisor Name" required>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Company Supervisor Phone Number</label>
                            <input type="text" name="csphone" class="form-control" placeholder="Company Supervisor Phone Number eg. 08012345678" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Company Supervisor Email</label>
                            <input type="email" name="csemail" class="form-control" placeholder="Company Supervisor Email" required>
                        </div>
                    </div>
                </div>

                <h3 class="section-title">School Supervisor Information</h3>
                <div class="form-group">
                    <label for="supervisor-id">Supervisor ID</label>
                    <input type="text" name="supervisor_id" class="form-control" placeholder="Deparment Supervisor Id" required>
                </div>
                <button type="submit" class="btn btn-two">REGISTER</button>

            </form>

        </div>

    </div>

</div>