<?php

use function App\get_db;

include 'required/Database.php';
include 'required/utils.php';

$request_method = $_SERVER['REQUEST_METHOD'];
$is_register_page  = true;


include("topnav.php")
?>

<?php

if (request_is("POST") and isset($_POST['supervisor'])) {
    $data = escape_post_data();
    $db = get_db();
    $email = $data['email'];
    $username = $data['username'];
    $username_or_email_exists = $db->select("select * from supervisors where email = '$email' or username = '$username'");
    if (!$username_or_email_exists) {
        $data["password"] = password_hash($data['password'], PASSWORD_BCRYPT);
        $inserted =  $db->insert("supervisors", $data);
        if ($inserted) {
            redirect("login.php?type=super");
        } else {
            $error = "error occured";
            redirect("register_supervisor.php");
        }
    } else {
        redirect("register_supervisor.php?error=username or email already taken");
    }
}
?>


<div class="container">

    <div class="row register-menu">
        <div class="col-md-3">
            <nav id="sidebar">
                <ul class="list-unstyled components">

                    <li class="register-menu">
                        <a class="btn" href="register.php">Register As a Student</a>
                    </li>

                    <li class="register-menu">
                        <a class="btn" href="register_employer.php">Register as Employer</a>
                    </li>

                </ul>
            </nav>


        </div>
        <div class="col-md-9">

            <form method="post" role="form">

                <div class="row">
                    <h3 class="section-title ">Personal Information</h3>
                    <span>
                        <?php if (isset($_GET['error'])) : ?>
                        <p><span class="text-danger"><b><?php echo $_GET['error'] ?></b></span></p><br />
                        <?php endif ?>

                    </span>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="initial" class="form-control" placeholder="Title" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Full Name" required>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control"
                                placeholder="Choose a username, this will be given to student" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone" class="form-control"
                                placeholder="Phone Number eg. 08012345678" required>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control"
                                placeholder="Email eg. myname@email.com" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>School</label>
                            <input type="text" name="school" class="form-control"
                                placeholder="input the name of your school" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Department</label>
                            <select name="department" class="form-control" required>
                                <option value="">Select Department</option>
                                <?php foreach ([
                                    "Computer Science and Engineering",
                                    "Chemical Engineering", "Civil Engineering",
                                    "Electronics and Electrical Engineering", "Food Science and Technology",
                                    "Mechanical Engineering", "Material Science and Engineering",
                                    "Agricultural and Environmental Engineering", "Building", "Architecture",
                                    "Estate Management", "Quantity Surveying", "Fine and Applied Arts", "Urban and Regional Planning",
                                    "Zoology", "Botany", "Microbiology", "Chemistry", "BioChemistry"
                                ] as $department) : ?>

                                <option value="<?php echo $department ?>"><?php echo $department ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>

                </div>


                <button type="submit" class="btn btn-two" name="supervisor">REGISTER</button>

            </form>

        </div>

    </div>

</div>

<?php include("footer.php") ?>