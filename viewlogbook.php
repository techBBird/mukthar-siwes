<?php
include("main_page_handler.php"); ?> <?php
                                        include("topnav.php");
                                        ?>
<?php
if (!can_view_log_book()) {
    redirect("login.php");
}
?>
<TABLE width="100%" cellpadding="0" cellspacing="0" border="0">
    <TR>
        <TD height="33" id="iPQLink" align="center">
            <div class="navi">
                <TABLE cellpadding="0" cellspacing="0" border="0" height="33">
                    <TR>

                        <TD width="114" align="center"><a href="supervisor.php"
                                style="color:white; text-decoration:none">Dashboard</a></TD>
                        <TD width="10">|</TD>


                        <TD width="114" id="Link"><a href="search_job.php"
                                style="color:white; text-decoration:none">Search Job</a></TD>
                    </TR>
                </TABLE>
            </div>
        </TD>
    </TR>
</TABLE>


<!-- container -->
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">


            <?php if (isset($_GET['message'])) : ?>
            <div class="alert alert-success" id="message-alert" role="alert">
                <?php echo $_GET['message'] ?>
            </div>
            <?php endif ?>

            <?php if (isset($grade) && $grade == true) : ?>
            <h3>Please Select Grade</h3>
            <form method="post" class="form-pt-2">
                <input type="hidden" name="matric" value="<?php echo $matric ?>">
                <input type="hidden" name="week" value="<?php echo $week ?>">
                <div class="form-row">
                    <div class="form-group">
                        <label for="week">Please Select Grade</label>
                        <select class="form-control" id="week" name="grade">
                            <?php foreach (range(0, 10) as $grade) : ?>
                            <option value="<?php echo $grade ?>"><?php echo $grade ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <input type="submit" name="grade_log" value="Grade" class="btn btn-primary btn-lg">

            </form>
            <?php endif ?>

            <!-- search logbook form -->
            <?php if (!isset($grade)) : ?>
            <form class="form-p2" method="post">
                <div class="form-group">
                    <label for="week">Please Select Week</label>
                    <select class="form-control" id="week" name="week">
                        <option value="All"> All</option>
                        <?php foreach (range(1, 24) as $week) : ?>
                        <option value="<?php echo $week ?>"><?php echo $week ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="week">Enter the student matric number</label>
                    <input class="form-control" type="text" name="matric" id="matric">
                </div>
                <button type="submit" class="btn btn-primary btn-lg" name="view-logs"> Views</button>
            </form>
            <?php endif ?>
            <!-- end search logbook -->





            <!-- display logbook -->
            <?php if (isset($logbooks)) : ?>
            <?php $count = 0 ?>

            <?php foreach ($logbooks as $workdone) : ?>

            <?php $week = $workdone["week"];
                    unset($workdone["week"]); ?>
            <?php if (is_supervisor() && isset($workdone['grade'])) {
                        $grade = $workdone['grade'];
                        unset($workdone['grade']);
                    } ?>


            <table class="table">
                <h3> Workdone for week <?php echo $week ?></h3>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Day</th>
                        <th scope="col">Date</th>
                        <th scope="col">Workdone</th>
                    </tr>
                    <?php if (isset($grade)) : ?>
                    <th scope="col">Grade</th>
                    <?php endif ?>

                </thead>
                <?php foreach ($workdone  as $day => $data) : ?>

                <tbody>
                    <tr>
                        <th scope="row"><?php echo $week ?></th>
                        <td><?php echo $day ?></td>
                        <td><?php echo $data[1] ?></td>
                        <td><?php echo $data[0] ?></td>
                        <?php if (isset($grade)) : ?>
                        <td><?php echo $grade ?></td>
                        <?php endif ?>

                    </tr>

                </tbody>

                <?php endforeach ?>

                <a class="btn btn-lg btn-primary" href="?action=grade&week=<?php echo $week ?>">Grade</a>

                <?php endforeach ?>
            </table>


            <?php endif ?>
            <!-- end display logbook -->
        </div>
        <div class="col-md-2"></div>

    </div>
</div>
<!-- /container -->
<?php include("footer.php") ?>