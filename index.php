<?php
$show_home_nav = true;
include("required/utils.php");
$is_register_page = false;
include("topnav.php");
?>
<!-- /.navbar -->


<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="grey-box-icon">
                <div class="icon-box-top grey-box-icon-pos">
                    <img src="assets/images/1.png" alt="" />
                </div>
                <!--icon box top -->
                <h4>Online SIWES</h4>
                <p>SIWES Log Book filling is made easy for you just a few clicks and you got your repots submitted.
                </p>

            </div>
            <!--grey box -->
        </div>
        <!--/span3-->
        <div class="col-md-3">
            <div class="grey-box-icon">
                <div class="icon-box-top grey-box-icon-pos">
                    <img src="assets/images/2.png" alt="" />
                </div>
                <!--icon box top -->
                <h4>Easy to Use</h4>
                <p>The platform is so easy to use, you can log in anytime, anywhere to fill your Log Book.</p>

            </div>
            <!--grey box -->
        </div>
        <!--/span3-->
        <div class="col-md-3">
            <div class="grey-box-icon">
                <div class="icon-box-top grey-box-icon-pos">
                    <img src="assets/images/3.png" alt="" />
                </div>
                <!--icon box top -->
                <h4>Write Less</h4>
                <p>With the use of the platform you write less and thus save the stress of paper work.</p>

            </div>
            <!--grey box -->
        </div>
        <!--/span3-->
        <div class="col-md-3">
            <div class="grey-box-icon">
                <div class="icon-box-top grey-box-icon-pos">
                    <img src="assets/images/4.png" alt="" />
                </div>
                <!--icon box top -->
                <h4>Ready?</h4>
                <p>Register now with your correct details and be online.<br> <a href="register.php"><em>Register Now
                            →</em></a></p>

            </div>
            <!--grey box -->
        </div>
        <!--/span3-->
    </div>
</div>



<section class="container main">
    <div class="row">
        <div class="col-md-8">
            <div class="title-box clearfix ">
                <h2 class="title-box_primary">About</h2>
            </div>
            <p><span>This website is designed for students to easily fill the SIWES log books online.</span></p>
            <p>In the digital age, a platform is needed to make SIWES log book filling easy and this plat form is
                here to serve the purpose.</p>
            <p>This project work was done by <b>Balogun Murtadho Abiodun - CSC/2016/123</b>, and supervised by <b>Prof.
                    H.A.
                    Soriyan
                </b> in fulfilment for the certificate of Bachelor of Science (B.Sc) in Computer
                Science and Engineering Department of Obafemi Awolowo University, Ile Ife, Osun State, Nigeria. </p>
            <a href="about.php" title="Read more..." class="btn-inline " target="_self">READ MORE...</a>
        </div>

        <center>
            <div class="col-md-4">
                <div class="title-box clearfix ">
                    <h2 class="title-box_primary">Project Objectives</h2>
                </div>
                <div class="list styled custom-list">
                    <ul>
                        <li>Speed</li>
                        <li>Neat Report</li>
                        <li>Less Paper Work</li>
                        <li>Any time Response</li>
                    </ul>
                </div>
            </div>
        </center>
    </div>
</section>
<?php include("footer.php"); ?>