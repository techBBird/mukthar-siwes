<?php

use function App\get_db;

include("required/utils.php");
include("required/Database.php");

$is_register_page  = true;
include("topnav.php");
$session = Session::getInstance();
$authorized = $session->isEmployer;
if (!loggedin() || !$authorized) {
    redirect("login.php?type=em");
}
$user_id = $session->user['id'];
$job_posting_error = false;

function create_job_posting($user_id)
{
    $data = escape_post_data();
    // echo $data['closed_at'];
    if (is_empty_array($data)) {
        return [false, true];
    }
    $job_data = array_map("escape_input", $data);
    $job_data['user_id'] = $user_id;
    $job_data['paid'] = strtolower($data['paid']) == 'true' ? 1 : 0;
    $closed_at = DateTime::createFromFormat("Y-m-d", $job_data['closed_at'], new DateTimeZone("Africa/Lagos"))->format("Y-m-d H:i:s");
    $job_data['closed_at'] = $closed_at;


    $db = get_db();

    // // try{
    $created_job = $db->insert("job_postings", $job_data);
    return [$created_job, false];
    // // }catch(\PDOException $e){
    // //     return [false,true];
    // // }

}

if (request_is("POST")) {
    [$created_job, $error] = create_job_posting($user_id);
    if ($error) {
        $job_posting_error = true;
    }
    if ($created_job) {
        redirect("employers.php?message=job created successful");
    }
}

?>

<header id="head" class="secondary">
    <div class="container">
        <h1>CREATE JOB OPENINGS</h1>
        <p>Please fill the form for the job details!</p>
    </div>
</header>

<div class="container">

    <div class="row register-menu">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <form action="create_job.php" method="post" role="form">

                <div class="row">
                    <h3 class="section-title ">Create Job Postings</h3>
                    <?php if ($job_posting_error) echo "error creating job please fill the form properly or try again latter" ?>
                    <div class="col-md-4">

                        <div class="form-group">
                            <label>Job Title</label>

                            <input type="text" name="title" class="form-control" placeholder="Job Title" required>
                        </div>
                    </div>

                    <div class="col-md-4">

                        <div class="form-group">
                            <label>Location</label>

                            <input type="text" name="location" class="form-control" placeholder="Job Location" required>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Application Closing Date</label>
                            <input type="date" name="closed_at" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="tag">Job Tag</label>
                            <input type="text" name="tag" class="form-control" id="tag" value=""
                                placeholder="Input the Job tag">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Number of Openings</label>
                            <input type="number" name="number_of_openings" class="form-control" placeholder="1"
                                required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="paid">Paid</label>
                            <select name="paid" id="paid" class="form-control" required>
                                <option value="">Select if the job is Paid</option>
                                <option value="false">False</option>
                                <option value="true">True</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Job Description</label>
                            <textarea name="description" id="decription" cols="30" rows="10"
                                placeholder="Enter Job Descriptions"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Requirements</label>
                            <textarea name="requirements" id="requirements" cols="30" rows="10"
                                placeholder="Enter Job requirements"></textarea>

                        </div>
                    </div>
                </div>


                <button type="submit" class="btn btn-two" name="register_em">CREATE</button>

            </form>

        </div>
        <div class="col-md-2"></div>

    </div>

</div>

<script>
tinymce.init({
    selector: 'textarea#decription',
    height: 500,
    menubar: false,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | help',
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});

tinymce.init({
    selector: 'textarea#requirements',
    height: 500,
    menubar: false,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | help',
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});
</script>

<?php include("footer.php") ?>