<?php include("main_page_handler.php"); ?>
<?php
include("topnav.php");
?>



<TABLE width="100%" cellpadding="0" cellspacing="0" border="0">
    <TR>
        <TD height="33" id="iPQLink" align="center">
            <div class="navi">
                <TABLE cellpadding="0" cellspacing="0" border="0" height="33">
                    <TR>

                        <TD width="114" align="center"><a href="mypage.php"
                                style="color:white; text-decoration:none">Dashboard</a></TD>
                        <TD width="10">|</TD>
                        <TD width="134" class="Hover" id="Link"><a href="logbook.php"
                                style="color:white; text-decoration:none">Fill Log Book</a></TD>
                        <TD width="10">|</TD>
                        <TD width="134" class="Hover" id="Link"><a href="mylogbooks.php"
                                style="color:white; text-decoration:none">Log Books</a></TD>
                        <TD width="10">|</TD>

                        <TD width="114" id="Link"><a href="search_job.php"
                                style="color:white; text-decoration:none">Search Job</a></TD>
                    </TR>
                </TABLE>
            </div>
        </TD>
    </TR>
</TABLE>


<!-- container -->

<div class="container">

    <div class="row register-menu">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <form method="post" role="form">
                <?php if (isset($successMSG)) : ?>
                <div class="alert alert-success" id="message-alert" role="alert">
                    <?php echo $successMSG ?>
                </div>
                <?php endif ?>
                <?php if (isset($errMSG)) : ?>
                <div class="alert alert-warning" id="message-alert" role="alert">
                    <?php echo $errMSG ?>
                </div>
                <?php endif ?>

                <div class="row">
                    <h3>Fill in Your logs </h3>
                </div>



                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="week">Select Week</label>
                            <select class="form-control" id="week" name="week">

                                <?php foreach (range(1, 24) as $week) : ?>
                                <option value="<?php echo $week ?>"><?php echo $week ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="week">Select Day of Week</label>
                            <select class="form-control" id="day" name="day">

                                <?php foreach ([
                                    "Monday", "Tuesday", "Wednesday",
                                    "Thursday", "Friday", "Saturday"
                                ] as $day) : ?>
                                <option value="<?php echo $day ?>"><?php echo $day ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input type="date" name="date" id="date">
                        </div>
                    </div>

                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Workdone</label>
                            <textarea name="workdone" id="workdone" cols="30" rows="10"
                                placeholder="Enter Job Descriptions"></textarea>
                        </div>
                    </div>

                </div>


                <button type="submit" class="btn btn-two" name="submit">CREATE</button>

            </form>

        </div>
        <div class="col-md-2"></div>

    </div>

</div>

<script>
tinymce.init({
    selector: 'textarea#workdone',
    height: 500,
    menubar: false,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | help',
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});
</script>

<!-- /container -->



<?php include("footer.php") ?>