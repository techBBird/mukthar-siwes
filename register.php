<?php


use App\database;

use function App\get_connection;

include 'required/Database.php';
include 'required/utils.php';

$request_method = $_SERVER['REQUEST_METHOD'];
$is_register_page  = true;

?>

<?php include("topnav.php") ?>

<?php
if ($request_method == "POST" && isset($_POST["register_em"])) {
	[$data, $error] = get_request_data();
	if ($error) {
		$error = json_encode($error);
		redirect("register_employer.php?error=$error");
	}
	[$register, $error] = register_employee($data);
	if ($error) {
		redirect("register_employer.php?r_error=$error");
	}
	redirect("login.php?type=em");
}
?>

<header id="head" class="secondary">
    <div class="container">
        <h1>REGISTER</h1>
        <p>Please fill your details correctly!</p>
    </div>
</header>


<!-- container -->
<div class="container">
    <div class="row">
        <div class="col-md-8">


            <?php

			if ($request_method == 'POST') {
				$data = array_filter(array_map(function ($input) {
					return mysqli_escape_string(get_connection(), $input);
				}, $_POST), function ($input) {
					return !is_null($input) && (is_string($input) ? $input != '' : !empty($input));
				});
				// var_dump($data);
				if (count($data) > 0) {

					[$success, $error] = register_user($data, new database());
					if ($error) {
						redirect("register.php?error=$error");
					} else {

						redirect("login.php");
					}
				} else {
					redirect("register.php?error=validation_error");
				}
			}
			?>

            <?php include("register_form.php") ?>
        </div>
    </div>
</div>
<!-- /container -->

<?php include("footer.php") ?>