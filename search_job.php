<?php include("main_page_handler.php");?>
<?php 
include("topnav.php"); 
?>


<TABLE width="100%" cellpadding="0" cellspacing="0" border="0">
    <TR>
        <TD height="33" id="iPQLink" align="center">
            <div class="navi">
                <TABLE cellpadding="0" cellspacing="0" border="0" height="33">
                    <TR>

                        <TD width="114" align="center"><a href="mypage.php"
                                style="color:white; text-decoration:none">Dashboard</a></TD>
                        <TD width="10">|</TD>
                        <TD width="134" class="Hover" id="Link"><a href="logbook.php"
                                style="color:white; text-decoration:none">Fill Log Book</a></TD>
                        <TD width="10">|</TD>
                        <TD width="134" class="Hover" id="Link"><a href="mylogbooks.php"
                                style="color:white; text-decoration:none">Log Books</a></TD>
                        <TD width="10">|</TD>

                        <TD width="114" id="Link"><a href="search_job.php"
                                style="color:white; text-decoration:none">Search Job</a></TD>
                    </TR>
                </TABLE>
            </div>
        </TD>
    </TR>
</TABLE>


<!-- container -->
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?php if(isset($_GET['message'])):?>
            <div class="alert alert-success" id="message-alert" role="alert">
                <?php echo $_GET['message'] ?>
            </div>
            <?php endif?>


            <form method="post" class="form-p2">
                <h4>Search for IT placement</h4>
                
                <div class="form-group">
                    <label for="location">Location</label>
                    <input type="text" class="form-control" id="location" aria-describedby="locationHelp"
                        placeholder="Enter Your Prefer Location">
                    <small id="locationHelp" class="form-text text-muted">Enter the prefer place you would love to do
                        your IT.</small>
                </div>
                <div class="form-group">
                    <label for="job">Enter Job Title</label>
                    <input type="text" name="job" class="form-control" id="job"
                        placeholder="Enter the job title to search">
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

            <?php if(isset($jobs)):?>
            <div class="row">
                
                    <div class="row">
                    <?php foreach($jobs as $job):?>
                        <div class="col-md-6">
                            <a href="apply.php?job_id=<?php echo $job['id']?>">
                            <h3><?php echo $job['title']?></h3>
                            <p ><?php echo $job['location']?></p>
                            </a>
                            
                        </div>
                    <?php endforeach?>
                    </div>
              
            </div>

        </div>
        <?php endif?>


    </div>
    <div class="col-md-2"></div>

</div>
</div>
<!-- /container -->



<?php include ("footer.php")?>