<?php


use App\database;

use function App\get_db;

include 'required/Database.php';
include 'required/utils.php';

// $request_method = $_SERVER['REQUEST_METHOD'];
// $is_register_page  = true;

?>

<?php include("topnav.php") ?>

<?php 
if(isset($_POST['submit'])){
    $data = escape_post_data();

    $cv = $_FILES['cv'];
    $name = $cv['name'];
	$tmp_dir = $cv['tmp_name'];
	$imgSize = $cv['size'];
	$error = $cv['error'];

    if ($error || empty($tmp_dir) || $tmp_dir == '' | empty($cv['name'])) {
		$errMSG = "Seems the file you selected does not exists. Please Select Image File.";
	}

    $upload_dir = "user_images/cvs/"; // upload directory
	// var_dump($_FILES['user_image']);
	// $successMSG = $tmp_dir;

	$imgExt = strtolower(pathinfo($cv['name'], PATHINFO_EXTENSION)); // get image extension

	// valid image extensions
	$valid_extensions = array('pdf'); // valid extensions
    // rename uploading image
	$userpic = $data["firstname"].rand(1000, 1000000) . "." . $imgExt;
	if (!in_array($imgExt, $valid_extensions)) {
		$errMSG = "Sorry, only PDF files are allowed.";
	}
	if ($imgSize > 6000000) {
		$errMSG = "Sorry, your file is too large.";
	}

	$saved = move_uploaded_file($tmp_dir, $upload_dir . $userpic);
	if (!$saved) {
		$errMSG = "error image could not be saved....";
		// redirect("mypage.php?error=$errMSG");
	}
	
    if(!isset($errMSG)){
        $data['cv'] = $userpic;
        $save_application = get_db()->insert("job_applications",$data);
        if($save_application){
            $successMSG = "Job Application is Successful ...";
            redirect("mypage.php?message=$successMSG");

        }

    }
	

}
?>




<div class="container">

    <div class="row register-menu">
       
        <div class="col-md-9">
            <h3 class="text-center">Apply For a Job</h3>
            <form action="apply.php" method="post" role="form" enctype="multipart/form-data">

                <div class="row">
                    <h3 class="section-title ">Personal Information</h3>
                    <span>
                        <?php if (isset($errMSG)) : ?>
                            <p><span class="text-danger"><b><?php echo $errMSG ?></b></span></p><br />
                        <?php endif ?>

                       
                    </span>
                    <input type="hidden" name="job_id" value="<?php echo $_GET['job_id']?>">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Surname</label>
                            <input type="text" name="surname" class="form-control" placeholder="Surname" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Firstname</label>
                            <input type="text" name="firstname" class="form-control" placeholder="Firstname" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="md">Middlename</label>
                            <input type="text" name="middlename" id="mid" class="form-control" placeholder="Middlename" required>
                        </div>
                    </div>
                </div>

                

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Gender</label>
                            <select name="gender" class="form-control" required>
                                <option value="">Select Gender</option>
                                <option value="Female">Female</option>
                                <option value="Male">Male</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone" class="form-control" placeholder="Phone Number eg. 08012345678" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Email eg. myname@email.com" required>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sch">School</label>
                            <input type="text" name="school" id="sch" class="form-control" placeholder="Your School" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Department</label>
                            <select name="department" class="form-control" required>
                                <option value="">Select Department</option>
                                <?php foreach ([
                                    "Computer Science and Engineering",
                                    "Chemical Engineering", "Civil Engineering",
                                    "Electronics and Electrical Engineering", "Food Science and Technology",
                                    "Mechanical Engineering", "Material Science and Engineering",
                                    "Agricultural and Environmental Engineering", "Building", "Architecture",
                                    "Estate Management", "Quantity Surveying", "Fine and Applied Arts", "Urban and Regional Planning",
                                    "Zoology", "Botany", "Microbiology", "Chemistry", "BioChemistry"
                                ] as $department) : ?>

                                    <option value="<?php echo $department ?>"><?php echo $department ?></option>
                                <?php endforeach ?>

                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Level</label>
                            <select name="level" class="form-control" required>
                                <option value="">Select Level</option>
                                <option value="part 3">PART 3</option>
                                <option value="part 4">PART 4</option>
                            </select>
                        </div>
                    </div>
                </div>

               

                <h3 class="section-title">Let's Know why you should be considered</h3>

                <div class="form-group">
                    <label id="cover">Cover Letter</label>
                    <textarea class="form-control" name="cover_letter" placeholder="Cover Letter" id="cover" cols="30" rows="10"></textarea>
                </div>

                <div class="form-group">
                    <label id="cover">Upload Your CV</label>
                    <input type="file" name="cv" id="cv" class="form-control" placeholder="Upload your CV ">
                </div>

               
                <button type="submit" name="submit" class="btn btn-two">APPLY</button>

            </form>

        </div>

    </div>

</div>



<?php include("footer.php") ?>