<?php 
include("required/utils.php");
$is_register_page  =true;
include("topnav.php");
if(isset($_GET['r_error'])){
    $r_error = $_GET['r_error'];
}
if  (isset($_GET['error'])){
    $error = json_decode($_GET['error'],true);
    //  var_dump($error);
}else{
    $error =null;
}
?>

<header id="head" class="secondary">
    <div class="container">
        <h1>REGISTER</h1>
        <p>Please fill your details correctly!</p>
    </div>
</header>

<div class="container">

    <div class="row register-menu">
        <div class="col-md-3">
            <nav id="sidebar">
                <ul class="list-unstyled components">

                    <li class="register-menu">
                        <a class="btn" href="register.php">Register As Student</a>
                    </li>

                    <li class="register-menu">
                        <a class="btn" href="register_supervisor.php">Register as a Supervisor</a>
                    </li>

                </ul>
            </nav>


        </div>
        <div class="col-md-9">

            <form action="register.php" method="post" role="form">

                <div class="row">
                    <h3 class="section-title ">Register As Employer</h3>

                    <div class="col-md-6">
                        <span>
                            <?php if (isset($r_error)): ?>
                            <p><span class="text-danger"><b><?php echo $r_error?></b></span></p><br />
                            <?php endif ?>

                        </span>
                        <div class="form-group">
                            <label>Full Name</label>
                            <span>
                                <?php if ($error  &&isset($error['fname'])&& $error['fname']): ?>
                                <p><span class="text-danger"><b>full name is required</b></span></p><br />
                                <?php endif ?>

                            </span>
                            <input type="text" name="fname" class="form-control" placeholder="Full Name" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Company Name</label>
                            <span>
                                <?php if ($error &&isset($error['cname'])&&$error['cname']): ?>
                                <p><span class="text-danger"><b>company name is required</b></span></p><br />
                                <?php endif ?>
                            </span>

                            <input type="text" name="cname" class="form-control" placeholder="Company Name" required>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <span>
                                <?php if ($error && isset($error['email'])&& $error['email']): ?>
                                <p><span class="text-danger"><b>please input a valid email</b></span></p><br />
                                <?php endif ?>

                            </span>
                            <input type="email" name="email" class="form-control"
                                placeholder="Email eg. myname@email.com" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Password</label>
                            <span>
                                <?php if ($error && $error['password']): ?>
                                <p><span class="text-danger"><b>password length should be greater or equal to 8
                                            characters</b></span></p><br />
                                <?php endif ?>

                            </span>
                            <input type="password" name="password" class="form-control" placeholder="Choose a Password"
                                required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Industry</label>
                            <select name="industry" class="form-control" required>
                                <option value="">Select Industry</option>
                                <option value="Accountancy">Finance</option>
                                <option value="Architecture">Architecture</option>
                                <option value="Engineering">Engineering</option>
                                <option value="Information Technology">Information Technology</option>
                                <option value="Telecommunication"></option>
                                <option value="others">Others</option>
                            </select>
                        </div>
                    </div>

                </div>


                <button type="submit" class="btn btn-two" name="register_em">REGISTER</button>

            </form>

        </div>

    </div>

</div>

<?php include("footer.php")?>